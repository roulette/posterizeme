package main;
import processing.core.PApplet;
import processing.xml.*;
public class Settings {
	XMLElement x;
	PApplet p;
	public Settings(PApplet parent, String filename)
	{
		p = parent;
		x = (new XMLElement(p, filename));

	}
	public String getSetting(String setting)
	{
		if(x.hasAttribute(setting))
			return x.getAttribute(setting);
		return "";
	}
	
	
	
	//XML static functions, not related to settings but a convenient place to put these static functions
	static XMLElement getChildWithAttribute(XMLElement aXML, String name, String attribute, String value)
	{
		for(int i = 0; i < aXML.getChildCount(); i++)
		{
			XMLElement tempElt = aXML.getChildAtIndex(i);
			if( tempElt.getName() == name )
				if(tempElt.getStringAttribute(attribute) == value)
					return tempElt;
		}
		return null;
	}
}
