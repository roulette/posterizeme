package main;
import processing.video.*;
import processing.xml.XMLElement;
import processing.core.*;
public class Vidizer {
	PApplet p;
	Settings settings;
	Movie v;
	boolean context;
	
	float dur;
	String prefix;
	int width;
	int height;
	float counter;
	XMLElement xml;
	public Vidizer(PApplet parent)
	{
		p = parent;
		context = false;
	}
	public void process(String filename)
	{
		setContext(filename);
		while(context);
			pump();
		
	} 
	
	public void setContext(String filename)
	{
		settings = new Settings(p,filename);
		v = new Movie(p,settings.getSetting("videoname"));
		dur = ((float)Integer.parseInt(settings.getSetting("durinms")))/1000f;
		prefix = settings.getSetting("prefix");
		width = Integer.parseInt(settings.getSetting("width"));
		height = Integer.parseInt(settings.getSetting("height"));
		counter = 0;
		xml = new XMLElement();
		context = true;
	}
	
	public void freeContext()
	{
		context = false;
	}
	
	public boolean pump()
	{
		if(context)
		{
			if(counter*dur <= v.duration())
			{
				//read in and resize the image
				v.jump(counter*dur);
				v.read();
				PImage img = p.createImage(v.width,v.height,PApplet.RGB);
				img.copy(v, 0, 0, v.width, v.height, 0, 0, img.width,img.height);
				img.resize(width,height);
				((Pmain)p).poster.posterize(img);
				//save the image
				img.save(prefix+zfill((new Integer((int)counter).toString()),5) + ".png");
				counter++;
				return true;
			}
			else
			{
				PApplet.println("done processing");
				//save xml
				freeContext();
				return false;
			}
		}
		return false;
	}
	private String zfill(String m, int l)
	{
	    String r = "";
	    for(int i = 0; i < l - m.length(); i++)
	    {
	      r+="0";
	    }
	    r+=m;
	    return r;
	}
		
}
