package main;

import processing.core.*;

public class ImageBox {
	PApplet p;
	Vector2di pos;
	int boxWidth = 255;
	int boxHeight = 200;
	PImage img = null;
	public ImageBox(PApplet parent, Vector2di _pos)
	{
		p = parent;
		pos = _pos.clone();
	}
	public void setImage(PImage _img)
	{
		img = _img;
	}
	public void draw()
	{
		p.pushMatrix();
		p.translate(pos.x, pos.y);
		if(img != null)
			p.image(img, 0,0,boxWidth, boxHeight);
		
		p.strokeWeight(1);
		p.stroke(127);
		p.line(0, 0, boxWidth, 0);
		p.line(0, 0, 0, boxHeight);
		p.line(0, boxHeight, boxWidth, boxHeight);
		p.line(boxWidth, 0, boxWidth, boxHeight);
		p.popMatrix();
	}
}
