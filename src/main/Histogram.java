package main;
import processing.core.*;
import java.util.Collections;
public class Histogram {
	PApplet p;
	Vector2di pos;
	Integer data[][] = new Integer[4][256];
	float scale = 1;
	float maxheight = 200f;
	public Histogram(PApplet parent, Vector2di _pos)
	{
		p = parent;
		pos = _pos.clone();
		for(Integer[] i : data)
			for(int j = 0; j < i.length; j++)
				i[j] = 0;
	}
	public void setData(PImage img)
	{
		img.loadPixels();
		for(int i : img.pixels)
		{
			data[0][(int)p.red(i)]++;
			data[1][(int)p.green(i)]++;
			data[2][(int)p.blue(i)]++;
			data[3][(int)p.brightness(i)]++;
		}
	}
	
	public void draw()
	{
		p.pushMatrix();
		p.translate(pos.x, pos.y);
		p.strokeWeight(1);
		p.stroke(255,0,0);
		for(int i = 0; i < data[0].length - 1; i++ )
			p.line(scale*i, -data[0][i]*maxheight/max(), scale*(i+1), -data[0][i + 1]*maxheight/max());
		p.stroke(0,255,0);
		for(int i = 0; i < data[1].length - 1; i++ )
			p.line(scale*i, -data[1][i]*maxheight/max(), scale*(i+1), -data[1][i + 1]*maxheight/max());
		p.stroke(0,0,255);
		for(int i = 0; i < data[2].length - 1; i++ )
			p.line(scale*i, -data[2][i]*maxheight/max(), scale*(i+1), -data[2][i + 1]*maxheight/max());
		p.stroke(255);
		for(int i = 0; i < data[3].length - 1; i++ )
			p.line(scale*i, -data[3][i]*maxheight/max(), scale*(i+1), -data[3][i + 1]*maxheight/max());
		//draw a grey box around everything
		p.stroke(127);
		p.line(0, 0, scale*255, 0);
		p.line(0, 0, 0, -maxheight);
		p.line(0, -maxheight, scale*255, -maxheight);
		p.line(scale*255, 0, scale*255, -maxheight);
		p.popMatrix();
		
	}
	
	private float max()
	{
		int m = 0;
		for(Integer[] i : data)
			for(int j = 0; j < i.length; j++)
				if( i[j] > m )
					m = i[j];
		return m;
	}
	
}
