package main;
import processing.core.PApplet;
public class Color {
	Integer r, g, b;
	Color(int _r, int _g, int _b)
	{
		r = _r;
		g = _g;
		b = _b;
	}
	Color(Integer[] c)
	{
		r = c[0];
		g = c[1];
		b = c[2];
	}
	public Integer[] asArray()
	{
		Integer[] ret = {r,g,b};
		return ret;
	}
	public int toPColor(PApplet p) {
		//SWAP THESE GUYS AROUND FOR MAXIMUM FUN
		return p.color(r,g,b);
	}
}
