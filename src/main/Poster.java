package main;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PImage;

public class Poster {
	class Properties
	{
		List<Integer> clist[];
		@SuppressWarnings("unchecked")
		public Properties()
		{
			//setup up an array of linkedlists.
			//must do this way because java disallows you to directly make arrays of generics
			clist = new LinkedList[3];
			for(int i=0; i<3; i++)
				clist[i] = new LinkedList<Integer>();
		}
		public void set(Integer[][] _clist)
		{
			for(int i = 0; i < clist.length; i++)
			{
				clist[i].clear();
				for(int j = 0; j < _clist[i].length; j++)
					clist[i].add(_clist[i][j]);
				Collections.sort(clist[i]);
				//PApplet.println(clist[i]);
			}
			
		}
		@SuppressWarnings("unused")
		private Integer getComponentAWESOMEBUNNYVERSION(List<Integer> l, Integer value)
		{
			//could be more efficient
			if(l.size() == 0)
				return 0;
			if(value < l.get(0)/2)
				return 0;
			for(int i = 0; i < l.size(); i++)
			{
				if(i == l.size()-1)
					return l.get(i);
				if(value >= l.get(i) && value < (l.get(i)+l.get(i+1))/2)
					return l.get(i);
			}
			return 0;
		}
		private Integer getComponent(List<Integer> l, Integer value)
		{
			//could be more efficient
			if(l.size() == 0)
				return 0;
			for(int i = 0; i < l.size(); i++)
			{
				if(i == l.size()-1)
				{
					return l.get(i);
				}
				if(value < (l.get(i)+l.get(i+1))/2)
					return l.get(i);
			}
			return 0;
		}
		public Color approx(Color c)
		{
			Integer[] ret = new Integer[3];
			Integer[] ca = c.asArray();
			for(int i = 0; i < 3; i++)
			{
				ret [i] = getComponent(clist[i],ca[i]);
			}
			return new Color(ret);
		}
		
	}
	public Properties prop = new Properties();
	PApplet p;
	public Poster(PApplet parent)
	{
		p = parent;
	}
	//this function posterizes images based on this.r/g/b and saves to dir/
	public void posterize(PImage[] images, String dir, String prefix) 
	{
		for(int i = 0; i < images.length; i++)
		{
			posterize(images[i],dir+prefix+i+".jpg");
		}
	}
	
	public void posterize(PImage image)
	{
		image.loadPixels();
		for(int j = 0; j < image.pixels.length; j++)
			image.pixels[j] = prop.approx(new Color(
					(int)p.red(image.pixels[j]),
					(int)p.green(image.pixels[j]),
					(int)p.blue(image.pixels[j])
					)).toPColor(p);
		image.updatePixels();
	}
	public void posterize(PImage image, String filename)
	{
		posterize(image);
		image.save(filename);
	}
}
