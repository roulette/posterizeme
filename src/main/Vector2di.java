package main;

public class Vector2di {
	public int x = 0;
	public int y = 0;
	
	public Vector2di()
	{
		
	}
	
	public Vector2di(int ax, int ay)
	{
		x = ax;
		y = ay;
	}
	
	public void setZero()
	{
		x = 0;
		y = 0;
	}
	
	public Vector2di clone()
	{
		return new Vector2di(x,y);
	}
}
