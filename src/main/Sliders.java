package main;
import java.util.LinkedList;
import java.util.List;
import processing.core.PApplet;
import java.util.Collections;


//TODO do midpoint sliders
public class Sliders 
{
	class Slider
	{
		int lineWidth = 255;
		class Tri implements Comparable<Tri>
		{
			public int x;
			public Tri(int _x)
			{
				move(_x);
			}
			public void move(int _x)
			{
				x = _x;
			}
			public void draw(PApplet p)
			{
				p.pushMatrix();
				p.translate(x, 0);
				p.line(0, 0, -2, 4);
				p.line(0, 0, 2, 4);
				p.popMatrix();
			}
			public void correct()
			{
				if(x > 255)
					x = 255;
				else if (x < 0)
					x = 0;
			}
			public int compareTo(Tri o)
			{
				return (new Integer(x)).compareTo(new Integer(o.x));
			}
		}
		
		List<Tri> t = new LinkedList<Tri>();
		Vector2di pos;
		PApplet p;
		Tri sel = null;
		public Slider(PApplet parent, Vector2di _pos)
		{
			p = parent;
			pos = _pos;
		}
		public void mousePressed(int x, int y)
		{
			if(Math.abs(y-pos.y) < 10)
			{
				for(Tri e : t)
					if(Math.abs(x - (e.x - pos.x)) < 4)
					{
						sel = e; 
						break;
					}
				if(sel == null)
				{
					sel = new Tri(x);
					sel.correct();
					t.add(sel);
					Collections.sort(t);
				}
			}
			
		}
		public void mouseReleased(int x, int y)
		{	
			//removing does not effect order, no need to resort
			if(Math.abs(y -pos.y) > 20)
				t.remove(sel);
			sel = null;
		}
		public void mouseMoved(int x, int y)
		{
			if(sel != null)
			{
				sel.x = x;
				sel.correct();
				Collections.sort(t);
			}
		}
		public void draw()
		{
			//TODO draw some sort of line
			p.pushMatrix();
			p.stroke(255);
			p.translate(pos.x, pos.y);
			p.line(0, 0, 255, 0);
			for(Tri i : t)
				i.draw(p);
			p.popMatrix();
		}
		public Integer[] getValues()
		{
			Integer ret[] = new Integer[t.size()];
			for(int i = 0; i < t.size(); i++)
			{
				ret[i] = new Integer(t.get(i).x);
			}
			return ret;
		}
	}
	
	PApplet p;
	Slider s[];
	Vector2di pos;
	boolean mode;
	public Sliders(PApplet parent,Vector2di _pos)
	{
		p = parent;
		pos = _pos;
		s = new Slider[3];
		mode = false;
		//position is given relative to self
		for(int i = 0; i < s.length; i++)
			s[i] = new Slider(p, new Vector2di(0,30*i));
	}
	public void mousePressed(int x, int y)
	{
		for(int i = 0; i < s.length; i++)
			s[i].mousePressed(x-pos.x,y-pos.y);		
	}
	public void mouseReleased(int x, int y)
	{
		for(int i = 0; i < s.length; i++)
			s[i].mouseReleased(x-pos.x,y-pos.y);		
	}
	public void mouseMoved(int x, int y)
	{
		for(int i = 0; i < s.length; i++)
			s[i].mouseMoved(x-pos.x,y-pos.y);
	}
	public void shift()
	{
		mode = !mode;
	}
	public void draw()
	{
		p.pushMatrix();
		p.translate(pos.x, pos.y);
		p.fill(0);
		p.stroke(0);
		p.rect(-2,0,259,s.length*25);
		p.stroke(255);
		for(int i = 0; i < s.length; i++)
			s[i].draw();
		p.popMatrix();
	}
	public Integer[][] getValues()
	{
		Integer ret[][] = new Integer[3][];
		for(int i = 0; i < s.length; i++)
		{
			ret[i] = s[i].getValues();
		}
		return ret;
	}
	
}
