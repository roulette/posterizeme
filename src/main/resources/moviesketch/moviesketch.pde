/**
 * Loop. 
 * 
 * Move the cursor across the screen to draw. 
 * Shows how to load and play a QuickTime movie file.  
 */

import processing.video.*;

Movie myMovie;
PImage mcopy;

void setup() {
  size(640, 480, P2D);
  background(0);
  // Load and play the video in a loop
  myMovie = new Movie(this, "station.mov");
  myMovie.loop();
  myMovie.pause();
  mousePressed();
}

void mousePressed() {
  myMovie.jump(random(myMovie.duration()));
  myMovie.read();
  mcopy = createImage(myMovie.width,myMovie.height,RGB);
  mcopy.copy(myMovie,0,0,myMovie.width,myMovie.height,0,0,mcopy.width,mcopy.height);
  mcopy.resize(400,400);
}

void movieEvent(Movie myMovie) {
  println("movie event");
  myMovie.read();
}

void draw() {
  tint(255, 20);
 
  image(mcopy, mouseX-myMovie.width/2, mouseY-myMovie.height/2);
}
