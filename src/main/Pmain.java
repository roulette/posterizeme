package main;
import processing.core.PApplet;
import processing.core.PImage;

public class Pmain extends PApplet {
	private static final long serialVersionUID = 1L;
	public static void main(String args[]) 
	{
		PApplet.main(new String[] {"main.Pmain"} );
	}
	
	//PRESET COLORS
	Integer ps1[][] = { {0,255},{0,255},{0,255} };
	Integer ps2[][] = { {0,63,127,189,255},{0,63,127,189,255},{0,63,127,189,255} };
	String imageName = "colors.jpg";
	PImage img = loadImage(imageName);
	
	Poster poster;
	Histogram hist = new Histogram(this, new Vector2di(25,450));
	ImageBox box = new ImageBox(this, new Vector2di(25,25));
	Sliders slid = new Sliders(this, new Vector2di(25,475));
	Vidizer vid = new Vidizer(this); 
	public void setup()
	{
		size(305,560);
		background(0);
		poster = new Poster(this);
		poster.prop.set(ps2);
		box.setImage(img);
		box.draw();
		hist.setData(img);
		hist.draw();
		slid.draw();

		//vid.setContext("video.xml");
		
	}
	public void draw()
	{
		//vid.pump();
		stroke(255);
	}
	public void keyPressed()
	{
		if(keyCode == ESC)
			exit();
		if(keyCode == 10)
		{
			background(0);
			poster.prop.set(slid.getValues());
			poster.posterize(img, "posterized.jpg");
			box.setImage(img);
			box.draw();
			hist.setData(img);
			hist.draw();
			slid.draw();
			img = loadImage(imageName);
		}
		
		if (keyCode == SHIFT)
		{
			slid.shift();
		}
	}
	public void mousePressed()
	{
		slid.mousePressed(mouseX, mouseY);
		slid.draw();
	}
	public void mouseReleased()
	{
		slid.mouseReleased(mouseX, mouseY);
		slid.draw();
	}
	//this code is shot, mouse moved is not called when mouse button is down
	public void mouseDragged()
	{
		slid.mouseMoved(mouseX, mouseY);
		slid.draw();
	}
}
